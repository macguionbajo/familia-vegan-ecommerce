import axios from 'axios'

// const parseMapsUrl = (url) => {
//   var regex = new RegExp('@(.*),(.*),');
//   var lon_lat_match = url.match(regex);
//   var lng = parseFloat(lon_lat_match[1]);
//   var lat = parseFloat(lon_lat_match[2]);
//   return {lat, lng}
// }

// Product keys
const NAME = 'gsx$nombre',
BRAND = 'gsx$marca',
PRICE = 'gsx$preciounidadokg',
AMOUNT = 'gsx$cantidad',
TAGS = 'gsx$tags',
IMAGE_SRC = 'gsx$imagen',
CATEGORY ='gsx$categoría'

const BULK_CATEGORY = "granel"

const parseProduct = (entry) => {
  let result = {
    name: entry[NAME] ? entry[NAME].$t : '',
    brand: entry[BRAND].$t,
    price: entry[PRICE] ? Number(entry[PRICE].$t.replace(/[^\d]+/g, "")) : null,
    amount: entry[AMOUNT].$t ? entry[AMOUNT].$t : 0,
    tags: entry[TAGS].$t.split(',').map(tag => tag.trim()).filter(tag => tag.length),
    image: entry[IMAGE_SRC].$t,
    type: entry[CATEGORY].$t,
    id: entry.id.$t.split('/').pop()
  }
  // No brand to null. Display as "-- no brand --"
  result.brand = result.brand.length ? result.brand : null
  result.brand_display = result.brand === null ? '-- Sin marca --' : result.brand

  // Add no-image default image
  result.image = result.image.length ? result.image :
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAHlBMVEX09PTh4eH19fXg4ODk5OTw8PDs7Ozq6uru7u7n5+dZKxXMAAAELUlEQVR4nO2dWXKtMAwFwQMX9r/hB9RNMOARhCTyTv/kIxVQl4kHYZmuAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAsZ03o9eA6OfwyH360bXWz30kyd2HHvbq8LaibQdJ2V+C7Yf6RSdQsEZOxApGqWCsyJNK5qPVsEZCsHOS1tksBNFI2rsZTY8gaG0QxaCRjRj0ITS4/wvW0judguaKbjaID1f+xLEZG8/psZtV6OeKF3GbH2DHW9fbDN0WgTn/j0wvBtVYEjSM9MAwxZgKAMMW4ChDLyGxoQ/eGA0NMYPk3O9c9PH80nyGZo1A/e9l3WEiZM8DxmeZm3GH/Ib1jFNXZna0AzntTFZcigPj2E8f2M/HIoshqkEFYsih+Fu7b9XZOhvWNow4bdw85YVMBjmkqg0Kb4sHG2YaUKaFF+W5w3zefDnO5vnR/wwPxXh8YwOw1Oaz4Pbmzct8ryhLxhe+EdsivRxw/RgePW2821a/uZ5w8ILt/auZp7Ct8Qqb9g6/17XKA3BKnhK227yswirjvZtPc22yqwN912jxW6bQGW87xrx93mCuoBfNWs7JkKqIuaYeWcf04brn3ey1ITMsXrK7F9oWD1Ft+pUxPyeFXB8L1I5aJYsRnKbTcM9E5utyleQzUTVz2eSu8mKYb8km5jbLleI+xUZ4cJ+wHzgr8jqFzY85iNnfPe0vJn5uVff8mamuKMzGzrn27Xu9+3a4Bu2JddsWc3Erv4NaeWe3HTw6t9yV246Tkev3bB6V3UyfOWGDdvGU/HrNmzbFx8X0GzYvPE/asA14l+hubIhqqC4DS+UbsQc9Bpeq005S2g1vFx8c7LQani5uuikIWhohnR5wI3yqaOHnOG8ZLQpxVv1YYd1p5jhuiZOKN4sgNsrShl+F/0xxfsVfjtFoRE/yGqcfkdQwhgqyrRhmLY5tiJJjWbwvkfEcJeXOjyoNEWoVrYND4m3UJGqyla2DU+ZxUCRqoxY1DCWOv3pbsjqpCUNo4LfVqQrBBc0jAquioaylF/OMCG4KlKW8j9vmBjxk4LUSLUhm6CUIZ+gkCGjoIwhp6CIIaughCGvoIAhsyC/Ibcg+4jPLsjdhvyCzIYCgryGEoK8hoV93u83lDn+i9VQ4iGFIQwrDN2fN/z7bQhDGMIQhjCEYdnwvxrxPyKnljIadn6QgNOwMxKwGgoDwxZgKAMMW4ChDE/N2kiCoyA4eYTgPO/t/PPluyc6COrICc5kDybZ1jodhPP+u4LFg1qkofjXyR9jIgzFaaKl84Rkoen9FDci1Tmb0h5JqM6gNFo7m2R5zgVFbd8kW5nHZzJOx0MowNqhHHiL4+h2H7ARZvl4HvUHAtfD1yfpycyX6TPSfwCxE8o+JXhADwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAs/ANyGFT0fw3sTAAAAABJRU5ErkJggg=='
  
  // Amount to null if empty. Display integers as 'X units'
  result.amount = result.amount === '' ? null : isNaN(result.amount) ? result.amount : parseInt(result.amount)
  result.amount_display = isNaN(result.amount) ? result.amount : `${result.amount} unidad${result.amount === 1 ? '' : 'es'}`
  // Type
  result.type = result.type.length ? result.type : null
  result.type_display = result.type === null ? '-- Sin categoría --' : result.type
  result.is_bulk = result.type_display.toLowerCase().trim() === BULK_CATEGORY

  // No tags? Add the 'no tags' tag
  result.tags = result.tags.length === 0 ? ['-- Sin tags --'] : result.tags

  return result
}

// Settings keys
const KEY = 'gsx$setting',
VALUE = 'gsx$valor'

const parseSettings = (entry) => {
  let result = {
    instagram: entry.find(item => item[KEY].$t.toLowerCase().trim() === "instagram")[VALUE].$t,
    facebook: entry.find(item => item[KEY].$t.toLowerCase().trim() === "facebook")[VALUE].$t,
    whatsapp: entry.find(item => item[KEY].$t.toLowerCase().trim() === "whatsapp")[VALUE].$t,
    address: entry.find(item => item[KEY].$t.toLowerCase().trim() === "dirección")[VALUE].$t,
    shipping: parseInt(entry.find(item => item[KEY].$t.toLowerCase().trim() === "precio despacho")[VALUE].$t),
    email: entry.find(item => item[KEY].$t.toLowerCase().trim() === "email")[VALUE].$t,
  }

  result.email = result.email.toLowerCase().trim()
  // result.mapsLocation = parseMapsUrl(result.mapsUrl)
  return result
}

export default {
  $_getSheetId() {
    return process.env.VUE_APP_SHEET_ID
  },
  fetchProducts() {
    const url = `https://spreadsheets.google.com/feeds/list/${this.$_getSheetId()}/1/public/values?alt=json`
    return axios.get(url).then(response => response.data.feed.entry.map(entry => parseProduct(entry)))
  },
  fetchProduct(id) {
    const url = `https://spreadsheets.google.com/feeds/list/${this.$_getSheetId()}/1/public/values/${id}?alt=json`
    return axios.get(url).then(response => parseProduct(response.data.entry))
  },
  fetchSettings() {
    const url = `https://spreadsheets.google.com/feeds/list/${this.$_getSheetId()}/2/public/values?alt=json`
    return axios.get(url).then(response => parseSettings(response.data.feed.entry))
  },

}