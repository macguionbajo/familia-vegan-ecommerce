export const [WHATSAPP, EMAIL] = ['WhatsApp',"Email"];

const whatsapp = {
    name: WHATSAPP,
    icon: 'mdi-whatsapp',
    component: 'sender-whatsapp',
    send: (phone, message) => {
        const url = encodeURI(
          `https://web.whatsapp.com/send?phone=${phone}&text=${message}&app_absent=0`
        )
        console.log("url", url)
        window.open(url, '_blank')
    }
}
const email = {
    name: EMAIL,
    icon: 'mdi-email',
    component: 'sender-email',
    send: (companyEmail, subject, message) => {
        const url = `mailto:${companyEmail}?subject=${encodeURI(subject)}&body=${encodeURI(message)}` 
        window.location.href = url
    }
}

export default {
    email,
    whatsapp
}