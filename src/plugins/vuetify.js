import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import es from 'vuetify/lib/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
      options: {
        customProperties: true,
      },
    themes: {
      light: {
        primary: '#34ADBC',
        secondary: '#4C5051',
        accent: '#E0E0E0',
        error: '#D32F2F', //'#050302',
        info: '#FFFEFF',
        success: '#7a904a',
        warning: '#f78012',
      },
    },
  },
    lang: {
      locales: { es },
      current: 'es',
    },
});


/*




                primary: '#ee44aa',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
        */