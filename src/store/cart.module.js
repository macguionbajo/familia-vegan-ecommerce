/* eslint-disable no-shadow */
const getDefaultState = () => ({
  products: {}
});

// initial state
const state = getDefaultState();

const actions = {
  set({commit}, {id, amount}) {
    commit('set', {id, amount})
  },
  add({commit}, id) {
    commit('add', {id, amount: 1})
  },
  addCustom({commit}, {id, amount}) {
    commit('add', {id, amount})
  },
  substract({ commit }, {id, amount}) {
    commit('substract', {id, amount})
  },
  increase({commit}, {id, amount}){
    commit('increase', {id, amount})
  },
  pop({commit}, id) {
    commit('pop', id)
  },
  remove({commit}, id) {
    commit('remove', id)
  },
  reset({commit}){
    commit('reset')
  }
};

const mutations = {
  reset(state) {
    Object.assign(state, getDefaultState());
  },
  set(state, {id, amount}) {
    state.products[id] = amount
    state.products = {...state.products}
  },
  add(state, {id, amount}) {
    state.products[id] = state.products[id] === undefined ? amount : state.products[id] + amount
    state.products = {...state.products}
  },
  substract(state, {id, amount}) {
    if (state.products[id] === undefined) return
    state.products[id] -= amount
    if (state.products[id] <= 0) {
      delete state.products[id]
    }
  },
  increase(state, {id, amount}){
    state.products[id] += amount
    state.products = {...state.products}
  },
  pop(state, id) {
    if (state.products[id] === undefined)
      return
    state.products[id] = state.products[id] - 1
    if (state.products[id] <= 0) {
      delete state.products[id]
    }
    state.products = {...state.products}
  },
  remove(state, id) {
    delete state.products[id]
    state.products = {...state.products}
  }
};

const getters = {
  product: (state) => (id) => state.products[id] === undefined ? 0 : state.products[id],
  products: (state) => Object.keys(state.products).filter(key => state.products[key] > 0),
  productsWithZeros: (state) => Object.keys(state.products),
  total: (state, getters, rootState, rootGetters) => {
    const prods = rootGetters['products/all']
    return prods.map(p => p.price * getters['product'](p.id)).reduce((acc, v) => acc+v, 0)
  },
  compiledMessage: (state, getters, rootState, rootGetters) => {
      // Compile cart as a message
      const cartProducts = getters['products']
      let message = "Hola! Quiero hacer un pedido por: \n"
      cartProducts.forEach(productId => {
        let product = rootGetters['products/byId'](productId)
        let amount = getters['product'](productId)
        let units = product.is_bulk ? ' Kg' : ''

        message +=
          `* ${amount}${units} x ${product.name} ${product.brand ? product.brand + " " : ''}${product.is_bulk ? '' : product.amount_display}\n`
      })
      const total = getters['total']
      message += `\nTotal: $${total.toLocaleString('es-CL')}`
      message += "\n Saludos 👋!"
      return message
    }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
