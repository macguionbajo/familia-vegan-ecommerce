import gSheet from '@/utils/gSheet'
/* eslint-disable no-shadow */
const getDefaultState = () => ({
  settings: {},
  lastFetch: 0,
});

// initial state
const state = getDefaultState();

const actions = {
  fetch({ commit }) {
    gSheet.fetchSettings().then(settings => {
      commit('update', settings)
    }).catch(error => {
      console.error("Failed to load settings: ", error)
    })
  }
};

const mutations = {
  update(state, newSettings) {
    state.settings = newSettings
    // Object.assign(state.products, items)
    state.lastFetch = Date.now() / 1000.0
  }
};

const getters = {
  instagram: (state) => state.settings.instagram,
  facebook: (state) => state.settings.facebook,
  whatsapp: (state) => state.settings.whatsapp,
  shipping: (state) => state.settings.shipping,
  email: (state) => state.settings.email
  // mapsLocation: (state) => state.settings.mapsLocation
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
