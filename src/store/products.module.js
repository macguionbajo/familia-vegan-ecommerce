import gSheet from '@/utils/gSheet'
/* eslint-disable no-shadow */
const getDefaultState = () => ({
  products: [],
  lastFetch: 0,
  product: {},
  fetching: false
});

// initial state
const state = getDefaultState();

const actions = {
  fetchItems({ commit }) {
    commit('setFetching', true)
    gSheet.fetchProducts().then(products => {
      commit('updateItems', products)
    }).catch(error => {
      console.error("Failed", error)
    }).finally(() => {
      commit('setFetching', false)
    })
  },
  fetchItem({ commit }, id) {
    gSheet.fetchProduct(id).then(product => {
      commit('updateItem', product)
    })
  }
};

const mutations = {
  updateItems(state, items) {
    state.products = items
    // Object.assign(state.products, items)
    state.lastFetch = Date.now() / 1000.0
  },
  updateItem(state, item) {
    state.product = item
  },
  setFetching(state, bool) {
    state.fetching = bool
  }
};

const getters = {
  all: (state) => state.products,
  byId: (state) => (id) => state.products.find(item => item.id === id),
  lastUpdate: (state) => state.lastFetch,
  brands: (state) => [...new Set(state.products.map(item => item.brand))]
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
