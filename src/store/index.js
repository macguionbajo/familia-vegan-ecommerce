import Vue from 'vue';
import Vuex from 'vuex';
// import createPersistedState from 'vuex-persistedstate';

import cart from './cart.module';
import products from './products.module';
import settings from './settings.module';

Vue.use(Vuex);

export default new Vuex.Store({
  // plugins: [createPersistedState()],
  plugins: [],
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    cart,
    products,
    settings,
  },
});
