import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import store from './store'

Vue.config.productionTip = false

const moment = require('moment')
require('moment/locale/es')

Vue.use(require('vue-moment'), {
    moment
})

Vue.prototype.$moment.locale('es')

Vue.filter('phone', (phone) => {
  return `${phone}`.replace(/[^0-9]/g, '').replace(/(\d{2})(\d)(\d)(\d{3})(\d{4})/, '(+$1 $2) $3 $4-$5');
})
Vue.filter('currency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('es-CL', {
        style: 'currency',
        currency: 'CLP',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });
    return formatter.format(value).replace(/\d(?=(?:\d{3})+$)/g, '$&.');
});

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
