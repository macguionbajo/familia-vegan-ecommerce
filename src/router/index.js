import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home'
import Shop from '@/views/Shop'
import Product from '@/views/Product'
import About from '@/views/About'
import Cart from '@/views/Cart'
import Layout from '@/views/Layout'
import Finish from '@/views/Finish'

Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      component: Layout,
      children:[
        {
          path:'/',
          component:Home,
          name:'Home'
        },
        {
          path:'/shop',
          component:Shop,
          name:'Shop'
        },
        {
          path:'/product/:id',
          component:Product,
          name:'Product'
        },
        {
          path:'/about',
          component:About,
          name:'About'
        },
        {
          path:'/cart',
          component:Cart,
          name:'Cart'
        },
        {
          path: '/finish',
          component: Finish,
          name: 'Finish'
        }
      ]
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.NODE_ENV === 'development' ? '/' : '/familia-vegan-ecommerce/',
  routes
})

export default router
